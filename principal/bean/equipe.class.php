<?php

    class Equipe{
        //Atributos
        private $nome;
        private $idequipe;
        private $idprojeto;

        //Construtor, getters e setters
        public function getNome(){
            return $this->nome;
        }

        public function setNome($nome){
            $this->nome = $nome;
        }

        public function getIdequipe(){
            return $this->idequipe;
        }

        public function setIdequipe($idequipe){
            $this->idequipe = $idequipe;
        }

        public function getIdprojeto(){
            return $this->idprojeto;
        }

        public function setIdprojeto($idprojeto){
            $this->idprojeto = $idprojeto;
        }

    }