<?php

    class Ata{
        //Atributos
        private $idata;
        private $descricao;

        //Construtor, getters e setters
        public function getIdAta(){
            return $this->idata;
        }

        public function setIdAta($id){
            $this->idata = $id;
        }

        public function getDescricao(){
            return $this->descricao;
        }

        public function setDescricao($descricao){
            $this->descricao = $descricao;
        }

    }