<?php

    class EquipeReuniaoAta{
        //Atributos
        private $idreuniao;
        private $idata;
        private $idequipe;

        //Construtor, getters e setters
        public function getIdReuniao(){
            return $this->idreuniao;
        }

        public function setIdReuniao($id){
            $this->idreuniao = $id;
        }

        public function getIdAta(){
            return $this->idata;
        }

        public function setIdAta($id){
            $this->idata = $id;
        }

        public function getIdEquipe(){
            return $this->idequipe;
        }

        public function setIdEquipe($id){
            $this->idequipe = $id;
        }

    }