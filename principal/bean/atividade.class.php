<?php

    class Atividade{
        //Atributos
        private $nome;
        private $idequipe;
        private $idatividade;
        private $descricao;
        private $prazo;
        private $prioridade;

        //Construtor, getters e setters
        public function getNome(){
            return $this->nome;
        }

        public function setNome($nome){
            $this->nome = $nome;
        }

        public function getIdequipe(){
            return $this->idequipe;
        }

        public function setIdequipe($idequipe){
            $this->idequipe = $idequipe;
        }

        public function getIdatividade(){
            return $this->idatividade;
        }

        public function setIdatividade($idatividade){
            $this->idatividade = $idatividade;
        }

        public function getDescricao(){
            return $this->descricao;
        }

        public function setDescricao($descricao){
            $this->descricao = $descricao;
        }

        public function getPrazo(){
            return $this->prazo;
        }

        public function setPrazo($prazo){
            $this->prazo = $prazo;
        }

        public function getPrioridade(){
            return $this->prioridade;
        }

        public function setPrioridade($prioridade){
            $this->prioridade = $prioridade;
        }

    }