<?php
    include_once 'usuario.class.php';
    include_once 'equipe.class.php';

    class UsuarioEquipe{
        private $idusuario;
        private $idequipe;
        private $tipoparticipacao;

        //Construtores, Getters e Setters
        public function getIdusuario(){
            return $this->idusuario;
        }

        public function setIdusuario($idusuario){
            $this->idusuario = $idusuario;
        }

        public function getIdequipe(){
            return $this->idequipe;
        }

        public function setIdequipe($idequipe){
            $this->idequipe = $idequipe;
        }

        public function getTipoparticipacao(){
            return $this->tipoparticipacao;
        }

        public function setTipoparticipacao($tipoparticipacao){
            $this->tipoparticipacao = $tipoparticipacao;
        }
    }