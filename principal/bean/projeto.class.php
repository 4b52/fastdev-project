<?php

    class Projeto{
        //Atributos
        private $id;
        private $nome;
        private $descricao;
        private $idcriador;

        //Construtor, getters e setters
        public function getNome(){
            return $this->nome;
        }

        public function setNome($nome){
            $this->nome = $nome;
        }

        public function getDescricao(){
            return $this->descricao;
        }

        public function setDescricao($descricao){
            $this->descricao = $descricao;
        }

        public function getId(){
            return $this->id;
        }

        public function setId($id){
            $this->id = $id;
        }

        public function getIdcriador(){
            return $this->idcriador;
        }

        public function setIdcriador($idcriador){
            $this->idcriador = $idcriador;
        }
    }