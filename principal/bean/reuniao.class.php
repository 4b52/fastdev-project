<?php

    class Reuniao{
        //Atributos
        private $idreuniao;
        private $datareuniao;
        private $horario;

        //Construtor, getters e setters
        public function getIdReuniao(){
            return $this->idreuniao;
        }

        public function setIdReuniao($id){
            $this->idreuniao = $id;
        }

        public function getDataReuniao(){
            return $this->datareuniao;
        }

        public function setDataReuniao($data){
            $this->datareuniao = $data;
        }

        public function getHorario(){
            return $this->horario;
        }

        public function setHorario($horario){
            $this->horario = $horario;
        }

    }