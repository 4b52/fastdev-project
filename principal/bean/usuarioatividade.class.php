<?php
    include_once 'usuario.class.php';
    include_once 'atividade.class.php';

    class UsuarioAtividade{
        private $idusuario;
        private $idatividade;

        //Construtores, Getters e Setters
        public function getIdusuario(){
            return $this->idusuario;
        }

        public function setIdusuario($idusuario){
            $this->idusuario = $idusuario;
        }

        public function getIdatividade(){
            return $this->idatividade;
        }

        public function setIdatividade($idatividade){
            $this->idatividade = $idatividade;
        }

    }