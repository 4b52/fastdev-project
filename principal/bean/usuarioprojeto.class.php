<?php
    include_once 'usuario.class.php';
    include_once 'projeto.class.php';

    class UsuarioProjeto{
        private $idusuario;
        private $idprojeto;
        private $tipoparticipacao;

        //Construtores, Getters e Setters
        public function getIdusuario(){
            return $this->idusuario;
        }

        public function setIdusuario($idusuario){
            $this->idusuario = $idusuario;
        }

        public function getIdprojeto(){
            return $this->idprojeto;
        }

        public function setIdprojeto($idprojeto){
            $this->idprojeto = $idprojeto;
        }

        public function getTipoparticipacao(){
            return $this->tipoparticipacao;
        }

        public function setTipoparticipacao($tipoparticipacao){
            $this->tipoparticipacao = $tipoparticipacao;
        }
    }