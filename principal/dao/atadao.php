<?php
    include_once '../models/connection.php';
    include_once '../bean/ata.class.php';

    class AtaDao{
        //retorna true, caso o cadastro tenha sucesso, ou false caso haja falha ao cadastrar
        public function criarAta($ata){
            $con = getConnection();
            $query = 'INSERT INTO ata (descricao) VALUES (:descricao)';
            $stmt = $con->prepare($query);
            $descricao = $ata->getDescricao();

            $stmt->bindParam(':descricao', $descricao);
            return $stmt->execute(); 
            $stmt = null;
            $con = null;
            die();
        }

        //Busca o id da última ata
        public function buscaIdUltimaAta(){
            $con = getConnection();
            $query = "SELECT idata FROM ata WHERE idata = (SELECT max(idata) FROM ata)";
            $stmt = $con->prepare($query);

            if($stmt->execute()){
                return $stmt->fetch();
            } else {
                return "Erro ao buscar as atividades";
            }
            $con = null;
            $stmt = null;
            die();
        }
    }