<?php
    include_once '../models/connection.php';
    include_once '../bean/reuniao.class.php';

    class ReuniaoDao{
        //retorna true, caso o cadastro tenha sucesso, ou false caso haja falha ao cadastrar
        public function agendarReuniao($reuniao){
            $con = getConnection();
            $query = 'INSERT INTO reuniao (datareuniao, horario) VALUES (:datareuniao, :horario)';
            $stmt = $con->prepare($query);
            $datareuniao = $reuniao->getDataReuniao();
            $horario = $reuniao->getHorario();

            $stmt->bindParam(':datareuniao', $datareuniao);
            $stmt->bindParam(':horario', $horario);
            return $stmt->execute(); 
            $stmt = null;
            $con = null;
            die();
        }

        //Busca as reuniões de uma equipe
        public function buscaReunioes($idequipe){
            $con = getConnection();
            $query = "SELECT r.datareuniao as data, r.horario as horario FROM projeto p INNER JOIN equipe e ON (p.idprojeto = e.idprojeto) INNER JOIN equipereuniaoata era ON (e.idequipe = era.idequipe) INNER JOIN reuniao r ON (era.idreuniao = r.idreuniao) WHERE e.idequipe = :idequipe ORDER BY r.datareuniao";
            $stmt = $con->prepare($query);

            $stmt->bindParam(':idequipe', $idequipe);

            if($stmt->execute()){
                return $stmt->fetchAll();
            } else {
                return "Erro ao buscar as atividades";
            }
            $con = null;
            $stmt = null;
            die();
        }

        //Busca o id da última reunião agendada
        public function buscaIdUltimaReuniao(){
            $con = getConnection();
            $query = "SELECT idreuniao FROM reuniao WHERE idreuniao = (SELECT max(idreuniao) FROM reuniao)";
            $stmt = $con->prepare($query);

            if($stmt->execute()){
                return $stmt->fetch();
            } else {
                return "Erro ao buscar as atividades";
            }
            $con = null;
            $stmt = null;
            die();
        }

        // //retorna true caso a alteração tenha sucesso, ou false, caso haja falha ao alterar
        // public function alteraAtividade($atividade){
        //     $con = getConnection();
        //     $nome = $atividade->getNome();
        //     $descricao = $atividade->getDescricao();
        //     $prazo = $atividade->getPrazo();
        //     $prioridade = $atividade->getPrioridade();
        //     $idatividade = $atividade->getIdrquipe();
        //     $idequipe = $atividade->getIdequipe();
                
        //     $query = 'UPDATE atividade SET nome = :nome, descricao = :descricao, prazo = :prazo, prioridade = :prioridade WHERE idatividade = :idatividade and idequipe = :idequipe';
    
        //     $stmt = $con->prepare($query);
        //     $stmt->bindParam(':nome', $nome);
        //     $stmt->bindParam(':descricao', $descricao);
        //     $stmt->bindParam(':prazo', $prazo);
        //     $stmt->bindParam(':prioridade', $prioridade);
        //     $stmt->bindParam(':idatividade', $idatividade);
        //     $stmt->bindParam('idequipe', $idequipe);
    
        //     return $stmt->execute();
        //     $con = null;
        //     $stmt = null;
        //     die();
        // }

        // //retorna true caso a exclusão tenha sucesso, ou false, caso haja falha ao excluir
        // public function excluiAtividade($atividade){
        //     $con = getConnection();
        //     $idatividade = $atividade->getIdatividade();
        //     $idequipe = $atividade->getIdequipe();
        //     $query = 'DELETE FROM atividade WHERE idatividade = :idatividade and idequipe = idequipe';
        //     $stmt = $con->prepare($query);

        //     $stmt->bindParam(':idatividade', $idatividade);
        //     $stmt->bindParam(':idequipe', $idequipe);
        //     return $stmt->execute();
        //     $con = null;
        //     $stmt = null;
        //     die();
        // }

        public function gerarRelatorioReunioes($idprojeto){
            $con = getConnection();
            $query = 'SELECT p.nome as projeto, e.nome as equipe, r.datareuniao as data, r.horario as horario FROM projeto p INNER JOIN equipe e ON (p.idprojeto = e.idprojeto) INNER JOIN equipereuniaoata era ON (e.idequipe = era.idequipe) INNER JOIN reuniao r ON (era.idreuniao = r.idreuniao) WHERE p.idprojeto = :idprojeto ORDER BY r.datareuniao';
            $stmt = $con->prepare($query);

            $stmt->bindParam(':idprojeto', $idprojeto);
            $stmt->execute();
            return $stmt->fetchAll();

            $con = null;
            $stmt = null;
            die();
        }
    }