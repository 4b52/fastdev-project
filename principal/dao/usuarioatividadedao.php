<?php
    include_once '../bean/usuarioatividade.class.php';
    include_once '../models/connection.php';

    class UsuarioAtividadeDao{
        public function cadastraNovoUsuarioAtividade($usuarioAtividade){
            $con = getConnection();
            $idusuario = $usuarioAtividade->getIdusuario();
            $idatividade = $usuarioAtividade->getIdatividade();

            $query = 'INSERT INTO usuarioatividade (idusuario, idatividade) VALUES (:idusuario, :idatividade)';

            $stmt = $con->prepare($query);
            $stmt->bindParam(':idusuario', $idusuario);
            $stmt->bindParam(':idatividade', $idatividade);

            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }

        public function buscaUsuarioAtividade(){
            $con = getConnection();

            $query = 'SELECT * FROM usuarioatividade';

            $stmt = $con->prepare($query);

            $stmt->execute();
            $resultado = $stmt->fetchAll();
            return $resultado;
            $con = null;
            $stmt = null;
            die();
        }

        // public function alteraUsuarioAtividade($usuarioAtividade){
        //     $con = getConnection();
        //     $idusuario = $usuarioAtividade->getIdusuario();
        //     $idatividade = $usuarioAtividade->getIdatividade();

        //     $query = 'UPDATE usuarioatividade SET tipoparticipacao = :tipoparticipacao WHERE idusuario = :idusuario AND idatividade = :idatividade';

        //     $stmt = $con->prepare($query);
        //     $stmt->bindParam(':idusuario', $idusuario);
        //     $stmt->bindParam(':idatividade', $idatividade);

        //     return $stmt->execute();
        //     $con = null;
        //     $stmt = null;
        //     die();
        // }

        public function deletaUsuarioAtividade($usuarioAtividade){
            $con = getConnection();
            $idusuario = $usuarioAtividade->getIdusuario();
            $idatividade = $usuarioAtividade->getIdatividade();

            $query = 'DELETE FROM usuarioatividade WHERE idusuario = :idusuario AND idatividade = :idatividade';

            $stmt = $con->prepare($query);
            $stmt->bindParam(':idusuario', $idusuario);
            $stmt->bindParam(':idatividade', $idatividade);

            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }
    }