<?php
    include_once '../models/connection.php';
    include_once '../bean/equipe.class.php';

    class EquipeDao{
        //retorna true, caso o cadastro tenha sucesso, ou false caso haja falha ao cadastrar
        public function cadastraNovaEquipe($equipe){
            $con = getConnection();
            $query = 'INSERT INTO equipe (nome, idprojeto) VALUES (:nome, :idprojeto)';
            $stmt = $con->prepare($query);
            $nome = $equipe->getNome();
            $idprojeto = $equipe->getIdprojeto();

            $stmt->bindParam(':nome', $nome);
            $stmt->bindParam(':idprojeto', $idprojeto);
            return $stmt->execute(); // execute retorna true ou false
            die();
        }

        //Busca o id de uma equipe pelo nome e id do projeto
        public function buscaIdEquipe($equipe){
            $con = getConnection();
            $nome = $equipe->getNome();
            $idprojeto = $equipe->getIdprojeto();
            $query = "SELECT idequipe FROM equipe WHERE nome = :nome and idprojeto = :idprojeto";
            $stmt = $con->prepare($query);

            $stmt->bindParam(':nome', $nome);
            $stmt->bindParam(':idprojeto', $idprojeto);

            if($stmt->execute()){
                return $stmt->fetch();
            } else {
                return "Erro ao buscar o id da equipe";
            }
            $con = null;
            $stmt = null;
            die();
        }

        //Busca as equipes ao selecionar um projeto
        public function buscaEquipes($equipe){
            $con = getConnection();
            $idprojeto = $equipe->getIdprojeto();
            $query = "SELECT * FROM equipe WHERE idprojeto = :idprojeto";
            $stmt = $con->prepare($query);

            $stmt->bindParam(':idprojeto', $idprojeto);

            if($stmt->execute()){
                return $stmt->fetchAll();
            } else {
                return "Erro ao buscar as equipes";
            }
            $con = null;
            $stmt = null;
            die();
        }

        //retorna true caso a alteração tenha sucesso, ou false, caso haja falha ao alterar
        public function alteraEquipe($equipe){
            $con = getConnection();
            $nome = $equipe->getNome();
            $idequipe = $equipe->getIdrquipe();
            $idprojeto = $equipe->getIdprojeto();
                
            $query = 'UPDATE equipe SET nome = :nome WHERE idequipe = :idequipe and idprojeto = :idprojeto';
    
            $stmt = $con->prepare($query);
            $stmt->bindParam(':nome', $nome);
            $stmt->bindParam(':idequipe', $idequipe);
            $stmt->bindParam('idprojeto', $idprojeto);
    
            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }

        //retorna true caso a exclusão tenha sucesso, ou false, caso haja falha ao excluir
        public function excluiEquipe($equipe){
            $con = getConnection();
            $idequipe = $equipe->getIdequipe();
            $idprojeto = $equipe->getIdprojeto();
            $query = 'DELETE FROM equipe WHERE idequipe = :idequipe and idprojeto = idprojeto';
            $stmt = $con->prepare($query);

            $stmt->bindParam(':idequipe', $idequipe);
            $stmt->bindParam(':idprojeto', $idprojeto);
            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }
    }
    