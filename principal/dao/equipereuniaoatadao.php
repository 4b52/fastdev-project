<?php
    include_once '../models/connection.php';
    include_once '../bean/equipereuniaoata.class.php';

    class EquipeReuniaoAtaDao{
        //retorna true, caso o cadastro tenha sucesso, ou false caso haja falha ao cadastrar
        public function criarEquipeReuniaoAta($era){
            $con = getConnection();
            $query = 'INSERT INTO equipereuniaoata (idequipe, idreuniao, idata) VALUES (:idequipe, :idreuniao, :idata)';
            $stmt = $con->prepare($query);
            $idreuniao = $era->getIdReuniao();
            $idequipe = $era->getIdEquipe();
            $idata = $era->getIdAta();

            $stmt->bindParam(':idreuniao', $idreuniao);
            $stmt->bindParam(':idequipe', $idequipe);
            $stmt->bindParam(':idata', $idata);
            return $stmt->execute(); 
            $stmt = null;
            $con = null;
            die();
        }
    }