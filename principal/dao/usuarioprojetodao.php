<?php
    include_once '../bean/usuarioprojeto.class.php';
    include_once '../models/connection.php';

    class UsuarioProjetoDao{
        public function criaNovoUsuarioProjeto($usuarioProjeto){
            $con = getConnection();
            $idusuario = $usuarioProjeto->getIdusuario();
            $idprojeto = $usuarioProjeto->getIdprojeto();
            $tipoparticipacao = $usuarioProjeto->getTipoparticipacao();

            $query = 'INSERT INTO usuarioprojeto (idusuario, idprojeto, tipoparticipacao) VALUES (:idusuario, :idprojeto, :tipoparticipacao)';

            $stmt = $con->prepare($query);
            $stmt->bindParam(':idusuario', $idusuario);
            $stmt->bindParam(':idprojeto', $idprojeto);
            $stmt->bindParam(':tipoparticipacao', $tipoparticipacao);

            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }

        public function buscaIdProjeto($usuarioprojeto){
            $con = getConnection();
            $idusuario = $usuarioprojeto->getIdusuario();
            $query = 'SELECT idprojeto FROM usuarioprojeto WHERE idusuario = :idusuario';

            $stmt = $con->prepare($query);
            $stmt->bindParam(':idusuario', $idusuario);

            $stmt->execute();
            $resultado = $stmt->fetchAll();
            return $resultado;
            $con = null;
            $stmt = null;
            die();
        }

        public function buscaUsuarioProjeto(){
            $con = getConnection();

            $query = 'SELECT * FROM usuarioprojeto';

            $stmt = $con->prepare($query);

            $stmt->execute();
            $resultado = $stmt->fetchAll();
            return $resultado;
            $con = null;
            $stmt = null;
            die();
        }

        public function alteraUsuarioProjeto($usuarioProjeto){
            $con = getConnection();
            $idusuario = $usuarioProjeto->getIdusuario();
            $idprojeto = $usuarioProjeto->getIdprojeto();
            $tipoparticipacao = $usuarioProjeto->getTipoparticipacao();

            $query = 'UPDATE usuarioprojeto SET tipoparticipacao = :tipoparticipacao WHERE idusuario = :idusuario AND idprojeto = :idprojeto';

            $stmt = $con->prepare($query);
            $stmt->bindParam(':idusuario', $idusuario);
            $stmt->bindParam(':idprojeto', $idprojeto);
            $stmt->bindParam(':tipoparticipacao', $tipoparticipacao);

            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }

        public function deletaUsuarioProjeto($usuarioProjeto){
            $con = getConnection();
            $idusuario = $usuarioProjeto->getIdusuario();
            $idprojeto = $usuarioProjeto->getIdprojeto();

            $query = 'DELETE FROM usuarioprojeto WHERE idusuario = :idusuario AND idprojeto = :idprojeto';

            $stmt = $con->prepare($query);
            $stmt->bindParam(':idusuario', $idusuario);
            $stmt->bindParam(':idprojeto', $idprojeto);

            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }
    }