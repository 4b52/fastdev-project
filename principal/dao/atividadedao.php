<?php
    include_once '../models/connection.php';
    include_once '../bean/atividade.class.php';

    class AtividadeDao{
        //retorna true, caso o cadastro tenha sucesso, ou false caso haja falha ao cadastrar
        public function cadastraNovaAtividade($atividade){
            $con = getConnection();
            $query = 'INSERT INTO atividade (nome, descricao, prazo, prioridade, idequipe) VALUES (:nome, :descricao, :prazo, :prioridade, :idequipe)';
            $stmt = $con->prepare($query);
            $nome = $atividade->getNome();
            $descricao = $atividade->getDescricao();
            $prazo = $atividade->getPrazo();
            $prioridade = $atividade->getPrioridade();
            $idequipe = $atividade->getIdequipe();

            $stmt->bindParam(':nome', $nome);
            $stmt->bindParam(':descricao', $descricao);
            $stmt->bindParam(':prazo', $prazo);
            $stmt->bindParam(':prioridade', $prioridade);
            $stmt->bindParam(':idequipe', $idequipe);
            return $stmt->execute(); 
            $stmt = null;
            $con = null;
            die();
        }

        //Busca as tarefas ao selecionar um projeto
        public function buscaAtividades($atividade){
            $con = getConnection();
            $idequipe = $atividade->getIdequipe();
            $query = "SELECT * FROM atividade WHERE idequipe = :idequipe";
            $stmt = $con->prepare($query);

            $stmt->bindParam(':idequipe', $idequipe);

            if($stmt->execute()){
                return $stmt->fetchAll();
            } else {
                return "Erro ao buscar as atividades";
            }
            $con = null;
            $stmt = null;
            die();
        }

        //Busca as tarefas ao selecionar um projeto
        public function buscaAtividade($atividade){
            $con = getConnection();
            $nome = $atividade->getNome();
            $idequipe = $atividade->getIdequipe();
            $query = "SELECT idatividade FROM atividade WHERE idequipe = :idequipe and nome = :nome";
            $stmt = $con->prepare($query);

            $stmt->bindParam(':idequipe', $idequipe);
            $stmt->bindParam(':nome', $nome);

            if($stmt->execute()){
                return $stmt->fetch();
            } else {
                return "Erro ao buscar as atividades";
            }
            $con = null;
            $stmt = null;
            die();
        }

        //retorna true caso a alteração tenha sucesso, ou false, caso haja falha ao alterar
        public function alteraAtividade($atividade){
            $con = getConnection();
            $nome = $atividade->getNome();
            $descricao = $atividade->getDescricao();
            $prazo = $atividade->getPrazo();
            $prioridade = $atividade->getPrioridade();
            $idatividade = $atividade->getIdrquipe();
            $idequipe = $atividade->getIdequipe();
                
            $query = 'UPDATE atividade SET nome = :nome, descricao = :descricao, prazo = :prazo, prioridade = :prioridade WHERE idatividade = :idatividade and idequipe = :idequipe';
    
            $stmt = $con->prepare($query);
            $stmt->bindParam(':nome', $nome);
            $stmt->bindParam(':descricao', $descricao);
            $stmt->bindParam(':prazo', $prazo);
            $stmt->bindParam(':prioridade', $prioridade);
            $stmt->bindParam(':idatividade', $idatividade);
            $stmt->bindParam('idequipe', $idequipe);
    
            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }

        //retorna true caso a exclusão tenha sucesso, ou false, caso haja falha ao excluir
        public function excluiAtividade($atividade){
            $con = getConnection();
            $idatividade = $atividade->getIdatividade();
            $idequipe = $atividade->getIdequipe();
            $query = 'DELETE FROM atividade WHERE idatividade = :idatividade and idequipe = idequipe';
            $stmt = $con->prepare($query);

            $stmt->bindParam(':idatividade', $idatividade);
            $stmt->bindParam(':idequipe', $idequipe);
            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }
    }
    