<?php
    include_once '../models/connection.php';
    include_once '../bean/projeto.class.php';

    class ProjetoDao{
        //retorna true, caso o cadastro tenha sucesso, ou false caso haja falha ao cadastrar
        public function cadastraNovoProjeto($projeto){
            $con = getConnection();
            $query = 'INSERT INTO projeto (nome, descricao, idcriador) VALUES (:nome, :descricao, :idcriador)';
            $stmt = $con->prepare($query);
            $nome = $projeto->getNome();
            $descricao = $projeto->getDescricao();
            $idcriador = $projeto->getIdcriador();
            
            $stmt->bindParam(':nome', $nome);
            $stmt->bindParam(':descricao', $descricao);
            $stmt->bindParam(':idcriador', $idcriador);
            return $stmt->execute(); 
            $con = null;
            $stmt = null;
            die();
        }

        //Busca nome de um projeto pelo id
        public function buscaNomeProjeto($projeto){
            $con = getConnection();
            $idprojeto = $projeto->getId();
            $query = "SELECT nome FROM projeto WHERE idprojeto = :idprojeto";
            $stmt = $con->prepare($query);

            $stmt->bindParam(':idprojeto', $idprojeto);

            if($stmt->execute()){
                return $stmt->fetchAll();
            } else {
                return "Erro ao buscar o id do usuário";
            }
            $con = null;
            $stmt = null;
            die();
        }

        //Busca o id de um projeto pelo nome
        public function buscaIdProjeto($projeto){
            $con = getConnection();
            $nome = $projeto->getNome();
            $idcriador = $projeto->getIdcriador();
            $query = "SELECT idprojeto FROM projeto WHERE nome = :nome and idcriador = :idcriador";
            $stmt = $con->prepare($query);

            $stmt->bindParam(':nome', $nome);
            $stmt->bindParam(':idcriador', $idcriador);

            if($stmt->execute()){
                return $stmt->fetch();
            } else {
                return "Erro ao buscar o id do usuário";
            }
            $con = null;
            $stmt = null;
            die();
        }

        //Busca o id de um projeto pelo nome
        public function buscaDadosProjeto($projeto){
            $con = getConnection();
            $nome = $projeto->getNome();
            $idcriador = $projeto->getIdcriador();
            $query = "SELECT idprojeto, descricao FROM projeto WHERE nome = :nome and idcriador = :idcriador";
            $stmt = $con->prepare($query);

            $stmt->bindParam(':nome', $nome);
            $stmt->bindParam(':idcriador', $idcriador);

            if($stmt->execute()){
                return $stmt->fetch();
            } else {
                return "Erro ao buscar o id do usuário";
            }
            $con = null;
            $stmt = null;
            die();
        }

        //retorna true caso a alteração tenha sucesso, ou false, caso haja falha ao alterar
        public function alteraProjeto($projeto){
            $con = getConnection();
            $nome = $projeto->getNome();
            $descricao = $projeto->getDescricao();
            $id = $projeto->getId();
                
            $query = 'UPDATE projeto SET nome = :nome, descricao = :descricao WHERE id = :id';
    
            $stmt = $con->prepare($query);
            $stmt->bindParam(':nome', $nome);
            $stmt->bindParam(':descricao', $descricao);
            $stmt->bindParam(':id', $id);
    
            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }

        public function excluiProjeto($projeto){
            $con = getConnection();
            $id = $projeto->getId();
            $query = 'DELETE FROM projeto WHERE id = :id';
            $stmt = $con->prepare($query);

            $stmt->bindParam(':id', $id);
            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }

        public function gerarRelatorio($projeto){
            $con = getConnection();
            $id = $projeto->getId();
            $query = 'SELECT p.nome as projeto, e.nome as equipe, u.nome as usuario, ue.tipoparticipacao as papel FROM projeto p INNER JOIN equipe e ON (p.idprojeto = e.idprojeto) INNER JOIN usuarioequipe ue ON (e.idequipe = ue.idequipe) INNER JOIN usuario u ON (ue.idusuario = u.idusuario) WHERE p.idprojeto = :idprojeto GROUP BY p.nome, e.nome, ue.tipoparticipacao, u.nome';
            $stmt = $con->prepare($query);

            $stmt->bindParam(':idprojeto', $id);
            $stmt->execute();
            return $stmt->fetchAll();

            $con = null;
            $stmt = null;
            die();
        }
    }
    