<?php
    include_once '../bean/usuarioequipe.class.php';
    include_once '../models/connection.php';

    class UsuarioEquipeDao{
        public function criaNovoUsuarioEquipe($usuarioEquipe){
            $con = getConnection();
            $idusuario = $usuarioEquipe->getIdusuario();
            $idequipe = $usuarioEquipe->getIdequipe();
            $tipoparticipacao = $usuarioEquipe->getTipoparticipacao();

            $query = 'INSERT INTO usuarioequipe (idusuario, idequipe, tipoparticipacao) VALUES (:idusuario, :idequipe, :tipoparticipacao)';

            $stmt = $con->prepare($query);
            $stmt->bindParam(':idusuario', $idusuario);
            $stmt->bindParam(':idequipe', $idequipe);
            $stmt->bindParam(':tipoparticipacao', $tipoparticipacao);

            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }

        public function buscaUsuariosEquipe($idequipe){
            $con = getConnection();

            $query = 'SELECT ue.idusuario, u.nome, ue.tipoparticipacao FROM usuarioequipe ue INNER JOIN usuario u ON (u.idusuario = ue.idusuario) WHERE idequipe = :idequipe';

            $stmt = $con->prepare($query);
            $stmt->bindParam(':idequipe', $idequipe);

            $stmt->execute();
            $resultado = $stmt->fetchAll();
            return $resultado;
            $con = null;
            $stmt = null;
            die();
        }

        public function alteraUsuarioEquipe($usuarioEquipe){
            $con = getConnection();
            $idusuario = $usuarioEquipe->getIdusuario();
            $idequipe = $usuarioEquipe->getIdequipe();
            $tipoparticipacao = $usuarioEquipe->getTipoparticipacao();

            $query = 'UPDATE usuarioequipe SET tipoparticipacao = :tipoparticipacao WHERE idusuario = :idusuario AND idequipe = :idequipe';

            $stmt = $con->prepare($query);
            $stmt->bindParam(':idusuario', $idusuario);
            $stmt->bindParam(':idequipe', $idequipe);
            $stmt->bindParam(':tipoparticipacao', $tipoparticipacao);

            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }

        public function deletaUsuarioEquipe($usuarioEquipe){
            $con = getConnection();
            $idusuario = $usuarioEquipe->getIdusuario();
            $idequipe = $usuarioEquipe->getIdequipe();

            $query = 'DELETE FROM usuarioequipe WHERE idusuario = :idusuario AND idequipe = :idequipe';

            $stmt = $con->prepare($query);
            $stmt->bindParam(':idusuario', $idusuario);
            $stmt->bindParam(':idequipe', $idequipe);

            return $stmt->execute();
            $con = null;
            $stmt = null;
            die();
        }
    }