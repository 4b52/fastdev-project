<?php
    include_once '../models/connection.php';
    include_once '../bean/usuario.class.php';

    class UsuarioDao{
        //retorna true, caso o cadastro tenha sucesso, ou false caso haja falha ao cadastrar
        public function cadastraNovoUsuario($usuario){
            $con = getConnection();
            $query = 'INSERT INTO usuario (nome, email, senha) VALUES (:nome, :email, :senha)';
            $stmt = $con->prepare($query);
            $nome = $usuario->getNome();
            $email = $usuario->getEmail();
            $senha = $usuario->getSenha();
            $stmt->bindParam(':nome', $nome);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':senha', $senha);
            return $stmt->execute(); // execute retorna true ou false
            die();
        }

        //Valida os logins dos usuários
        public function validaLogin($usuario){
            $con = getConnection();
            $email = $usuario->getEmail();
            $senha = $usuario->getSenha();
            $query = 'SELECT idusuario, nome FROM usuario WHERE email = :email and senha = :senha';

            $stmt = $con->prepare($query);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':senha', $senha);

            if($stmt->execute()){
                $resultado = $stmt->fetch(); 
                return $resultado;
            } else{
                return "Erro ao validar o login";
            }
            $con = null;
            $stmt = null;
            die();
            
        }

        //Busca o id de um usuário pelo email
        public function buscaIdUsuario($usuario){
            $con = getConnection();
            $email = $usuario->getEmail();
            $query = "SELECT idusuario FROM usuario WHERE email = :email";
            $stmt = $con->prepare($query);

            $stmt->bindParam(':email', $email);

            if($stmt->execute()){
                return $stmt->fetch();
            } else {
                return "Erro ao buscar o id do usuário";
            }
            $con = null;
            $stmt = null;
            die();
        }
    }
    