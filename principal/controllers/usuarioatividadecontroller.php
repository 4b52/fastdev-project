<?php
    include_once '../bean/usuarioatividade.class.php';
    include_once '../dao/usuarioatividadedao.php';

    class UsuarioAtividadeController{
        public function cadastraNovoUsuarioAtividade($idusuario, $idatividade){
            $usuarioatividade = new UsuarioAtividade();
            $usuarioatividade->setIdusuario($idusuario);
            $usuarioatividade->setIdatividade($idatividade);

            $usuarioatividadedao = new UsuarioAtividadeDao();
            return $usuarioatividadedao->cadastraNovoUsuarioAtividade($usuarioatividade);
        }

        public function buscaUsuarioAtividade(){
            $usuarioatividadedao = new UsuarioAtividadeDao();
            return $usuarioatividadedao->buscaUsuarioAtividade();
        }

        // public function alteraUsuarioAtividade($tipoparticipacao){
        //     $usuarioatividade = new UsuarioAtividade();
        //     $usuarioatividade->setTipoparticipacao($tipoparticipacao);

        //     $usuarioatividadedao = new UsuarioAtividadeDao();
        //     return $usuarioatividadedao->alteraUsuarioAtividade($usuarioatividade);
        // }

        public function deletaUsuarioAtividade($idusuario, $idatividade){
            $usuarioatividade = new UsuarioAtividade();
            $usuarioatividade->setIdusuario($idusuario);
            $usuarioatividade->setIdatividade($idatividade);

            $usuarioatividadedao = new UsuarioAtividadeDao();
            return $usuarioatividadedao->deletaUsuarioAtividade($usuarioatividade);        
        }
    }