<?php
    include_once '../bean/usuarioequipe.class.php';
    include_once '../dao/usuarioequipedao.php';

    class UsuarioEquipeController{
        public function criaNovoUsuarioEquipe($idusuario, $idequipe, $tipoparticipacao){
            $usuarioequipe = new UsuarioEquipe();
            $usuarioequipe->setIdusuario($idusuario);
            $usuarioequipe->setIdequipe($idequipe);
            $usuarioequipe->setTipoparticipacao($tipoparticipacao);

            $usuarioequipedao = new UsuarioEquipeDao();
            return $usuarioequipedao->criaNovoUsuarioEquipe($usuarioequipe);
        }

        public function buscaUsuariosEquipe($idequipe){
            $usuarioequipedao = new UsuarioEquipeDao();
            return $usuarioequipedao->buscaUsuariosEquipe($idequipe);
        }

        public function alteraUsuarioEquipe($tipoparticipacao){
            $usuarioequipe = new UsuarioEquipe();
            $usuarioequipe->setTipoparticipacao($tipoparticipacao);

            $usuarioequipedao = new UsuarioEquipeDao();
            return $usuarioequipedao->alteraUsuarioEquipe($usuarioequipe);
        }

        public function deletaUsuarioEquipe($idusuario, $idequipe){
            $usuarioequipe = new UsuarioEquipe();
            $usuarioequipe->setIdusuario($idusuario);
            $usuarioequipe->setIdequipe($idequipe);

            $usuarioequipedao = new UsuarioEquipeDao();
            return $usuarioequipedao->deletaUsuarioEquipe($usuarioequipe);        
        }
    }