<?php
    include_once '../dao/equipereuniaoatadao.php';
    include_once '../bean/equipereuniaoata.class.php';

    class EquipeReuniaoAtaController{
        public function criarEquipeReuniaoAta($idequipe, $idreuniao, $idata){
            $equipereuniaoatadao = new EquipeReuniaoAtaDao();
            $equipereuniaoata = new EquipeReuniaoAta();
            $equipereuniaoata->setIdEquipe($idequipe);
            $equipereuniaoata->setIdReuniao($idreuniao);
            $equipereuniaoata->setIdAta($idata);
            return $equipereuniaoatadao->criarEquipeReuniaoAta($equipereuniaoata);
            die();
        }
    }