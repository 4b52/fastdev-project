<?php
    include_once '../dao/atadao.php';
    include_once '../bean/ata.class.php';

    class AtaController{
        public function criarAta($descricao){
            $atadao = new AtaDao();
            $ata = new Ata();
            $ata->setDescricao($descricao);
            return $atadao->criarAta($ata);
            die();
        }

        public function buscaIdUltimaAta(){
            $atadao = new AtaDao();
            return $atadao->buscaIdUltimaAta();
            die();
        }
    }
