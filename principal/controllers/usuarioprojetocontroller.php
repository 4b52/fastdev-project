<?php
    include_once '../bean/usuarioprojeto.class.php';
    include_once '../dao/usuarioprojetodao.php';

    class UsuarioProjetoController{
        public function criaNovoUsuarioProjeto($idusuario, $idprojeto, $tipoparticipacao){
            $usuarioprojeto = new UsuarioProjeto();
            $usuarioprojeto->setIdusuario($idusuario);
            $usuarioprojeto->setIdProjeto($idprojeto);
            $usuarioprojeto->setTipoparticipacao($tipoparticipacao);

            $usuarioprojetodao = new UsuarioProjetoDao();
            return $usuarioprojetodao->criaNovoUsuarioProjeto($usuarioprojeto);
        }

        public function buscaIdProjeto($idusuario){
            $usuarioprojetodao = new UsuarioProjetoDao();
            $usuarioprojeto = new UsuarioProjeto();
            $usuarioprojeto->setIdusuario($idusuario);

            return $usuarioprojetodao->buscaIdProjeto($usuarioprojeto);
        }

        public function buscaUsuarioProjeto(){
            $usuarioprojetodao = new UsuarioProjetoDao();
            return $usuarioprojetodao->buscaUsuarioProjeto();
        }

        public function alteraUsuarioProjeto($tipoparticipacao){
            $usuarioprojeto = new UsuarioProjeto();
            $usuarioprojeto->setTipoparticipacao($tipoparticipacao);

            $usuarioprojetodao = new UsuarioProjetoDao();
            return $usuarioprojetodao->alteraUsuarioProjeto($usuarioprojeto);
        }

        public function deletaUsuarioProjeto($idusuario, $idprojeto){
            $usuarioprojeto = new UsuarioProjeto();
            $usuarioprojeto->setIdusuario($idusuario);
            $usuarioprojeto->setIdprojeto($idprojeto);

            $usuarioprojetodao = new UsuarioProjetoDao();
            return $usuarioprojetodao->deletaUsuarioProjeto($usuarioprojeto);        
        }
    }