<?php
    include_once '../dao/atividadedao.php';
    include_once '../bean/atividade.class.php';

    class AtividadeController{
        public function novaAtividade($nome, $descricao, $prazo, $prioridade, $idequipe){
            $atividadedao = new AtividadeDao();
            $atividade = new Atividade();
            $atividade->setNome($nome);
            $atividade->setDescricao($descricao);
            $atividade->setPrazo($prazo);
            $atividade->setPrioridade($prioridade);
            $atividade->setIdequipe($idequipe);
            return $atividadedao->cadastraNovaAtividade($atividade);
            die();
        }

        public function buscaAtividades($idequipe){
            $atividadedao = new AtividadeDao();
            $atividade = new Atividade();
            $atividade->setIdequipe($idequipe);
            return $atividadedao->buscaAtividades($atividade);
            die();
        }

        public function buscaAtividade($nomeatividade, $idequipe){
            $atividadedao = new AtividadeDao();
            $atividade = new Atividade();
            $atividade->setNome($nomeatividade);
            $atividade->setIdequipe($idequipe);
            return $atividadedao->buscaAtividade($atividade);
            die();
        }

        public function alteraAtividade($nome, $descricao, $prazo, $prioridade, $idequipe, $idatividade){
            $atividadedao = new AtividadeDao();
            $atividade = new Atividade();
            $atividade->setNome($nome);
            $atividade->setDescricao($descricao);
            $atividade->setPrazo($prazo);
            $atividade->setPrioridade($prioridade);
            $atividade->setIdatividade($idequipe);
            $atividade->setIdatividade($idatividade);
            return $atividadedao->alteraAtividade($atividade);
            die();
        }

        public function excluiAtividade($idequipe, $idatividade){
            $atividadedao = new AtividadeDao();
            $atividade = new Atividade();
            $atividade->setIdequipe($idequipe);
            $atividade->setIdatividade($idatividade);  
            return $atividadedao->excluiAtividade($atividade);
            die();
        }
    }