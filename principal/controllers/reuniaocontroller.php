<?php
    include_once '../dao/reuniaodao.php';
    include_once '../bean/reuniao.class.php';

    class ReuniaoController{
        public function agendarReuniao($datareuniao, $horario){
            $reuniaodao = new ReuniaoDao();
            $reuniao = new Reuniao();
            $reuniao->setDataReuniao($datareuniao);
            $reuniao->setHorario($horario);
            return $reuniaodao->agendarReuniao($reuniao);
            die();
        }

        public function buscarReunioes($idequipe){
            $reuniaodao = new ReuniaoDao();
            return $reuniaodao->buscaReunioes($idequipe);
            die();
        }

        public function buscaIdUltimaReuniao(){
            $reuniaodao = new ReuniaoDao();
            return $reuniaodao->buscaIdUltimaReuniao();
            die();
        }

        // public function alteraReuniao($nome, $descricao, $prazo, $prioridade, $idequipe, $idreuniao){
        //     $reuniaodao = new ReuniaoDao();
        //     $reuniao = new Reuniao();
        //     $reuniao->setNome($nome);
        //     $reuniao->setDescricao($descricao);
        //     $reuniao->setPrazo($prazo);
        //     $reuniao->setPrioridade($prioridade);
        //     $reuniao->setIdreuniao($idequipe);
        //     $reuniao->setIdreuniao($idreuniao);
        //     return $reuniaodao->alteraReuniao($reuniao);
        //     die();
        // }

        // public function excluiReuniao($idequipe, $idreuniao){
        //     $reuniaodao = new ReuniaoDao();
        //     $reuniao = new Reuniao();
        //     $reuniao->setIdequipe($idequipe);
        //     $reuniao->setIdreuniao($idreuniao);  
        //     return $reuniaodao->excluiReuniao($reuniao);
        //     die();
        // }

        public function gerarRelatorioReunioes($idprojeto){
            $reuniaodao = new ReuniaoDao();
            return $reuniaodao->gerarRelatorioReunioes($idprojeto);
        }
    }