<?php
    include_once '../dao/usuariodao.php';
    include_once '../bean/usuario.class.php';
    class UsuarioController{
        public function novoUsuario($nome, $email, $senha){
            $usuariodao = new UsuarioDao();
            $usuario = new Usuario();
            $usuario->setNome($nome);
            $usuario->setEmail($email);
            $usuario->setSenha($senha);
            return $usuariodao->cadastraNovoUsuario($usuario);
        }

        public function validaLogin($email, $senha){
            $usuariodao = new UsuarioDao();
            $usuario = new Usuario();
            $usuario->setEmail($email);
            $usuario->setSenha($senha);
            $dadosusuario = $usuariodao->validaLogin($usuario);
            $usuario->setNome($dadosusuario[1]);
            $usuario->setId($dadosusuario[0]);
            return $usuario;
        }

        public function buscaIdUsuario($email){
            $usuariodao = new UsuarioDao();
            $usuario = new Usuario();
            $usuario->setEmail($email);
            return $usuariodao->buscaIdUsuario($usuario);
        }
    }
    