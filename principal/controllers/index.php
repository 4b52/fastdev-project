<?php
    include_once 'usuariocontroller.php';
    include_once '../bean/usuario.class.php';

    include_once 'projetocontroller.php';
    include_once '../bean/projeto.class.php';

    include_once 'equipecontroller.php';
    include_once '../bean/equipe.class.php';

    include_once 'usuarioprojetocontroller.php';
    include_once '../bean/usuarioprojeto.class.php';

    include_once 'usuarioequipecontroller.php';
    include_once '../bean/usuarioequipe.class.php';

    include_once 'atividadecontroller.php';
    include_once '../bean/atividade.class.php';

    include_once 'usuarioatividadecontroller.php';
    include_once '../bean/usuarioatividade.class.php';

    include_once 'reuniaocontroller.php';
    include_once '../bean/reuniao.class.php';

    include_once 'atacontroller.php';
    include_once '../bean/ata.class.php';

    include_once 'equipereuniaoatacontroller.php';
    include_once '../bean/equipereuniaoata.class.php';

    include_once '../relatorios/criaPdf.php';
    require "../../vendor/autoload.php";


    session_start();
    // var_dump($_SESSION['userlogin']);

    if(isset($_POST['cadastrarse'])){
        if($_POST['senha'] != $_POST['repetirsenha']){
            echo '<script> alert("As senhas digitadas devem ser iguais"); history.go(-1) </script>';
            die();
        } else {
            $usuariocontroller = new UsuarioController();
            if($usuariocontroller->novoUsuario($_POST['nome'], $_POST['email'], $_POST['senha'])){
                echo '<script> alert("Cadastrado com sucesso"); window.location.href="../views/inicial.php"; </script>';
                die();
            } else {
                echo '<script> alert("Email já cadastrado."); history.go(-1)</script>';
                die();
            }
        }
        
    } else if(isset($_SESSION['userlogin'])){
        var_dump(unserialize($_SESSION['userlogin']));
        $usuariocontroller = new UsuarioController();
        $user = unserialize($_SESSION['userlogin']);
        if($usuariocontroller->novoUsuario($user->getName(), $user->getEmail(), "1234")){
            $resultado = $usuariocontroller->validaLogin($user->getEmail(), "1234");
            unset($_SESSION['userlogin']);

            if(isset($resultado)){
                $_SESSION['usuario'] = $resultado;
                header("Location: ../views/principal.php");
                exit();
            }  else {
                echo "<script> console.log('else'); </script>";
            }
            die();
        } else {
            $resultado = $usuariocontroller->validaLogin($user->getEmail(), "1234");
            $_SESSION['userlogin'] = null;
            if(isset($resultado)){
                $_SESSION['usuario'] = $resultado;
                header("Location: ../views/principal.php");
                exit();
            }  else {
                echo "<script> console.log('else'); </script>";
            }

            die();
        }
    } else if(isset($_POST['acessar'])){ 
        $usuariocontroller = new UsuarioController();
        $resultado = $usuariocontroller->validaLogin($_POST['email'], $_POST['senha']);
        if(isset($resultado)){
            $_SESSION['usuario'] = $resultado;
            header("Location: ../views/principal.php");
            exit();
        }  else {
            echo "<script> console.log('else'); </script>";
        }

    } else if($_POST['chave'] == "novo-projeto"){
        $usuariocontroller = new UsuarioController();
        $projetocontroller = new ProjetoController();
        $equipecontroller = new EquipeController();
        $usuarioprojetocontroller = new UsuarioProjetoController();
        $usuarioequipecontroller = new UsuarioEquipeController();
        
        $idusuario = $usuariocontroller->buscaIdUsuario($_SESSION['usuario']->getEmail());
        $idprojeto = $projetocontroller->criaNovoProjeto($_POST['nomeProjeto'], $_POST['descricaoProjeto'], $idusuario[0]);
        $idequipe = $equipecontroller->criaNovaEquipe("Administradores", $idprojeto[0]);
        $usuarioequipecontroller->criaNovoUsuarioEquipe($idusuario[0], $idequipe[0], "Admin");
        echo json_encode($usuarioprojetocontroller->criaNovoUsuarioProjeto($idusuario[0], $idprojeto[0], "Admin"));
        
    } else if($_POST['chave'] == "nova-equipe"){
        $usuariocontroller = new UsuarioController();
        $projetocontroller = new ProjetoController();
        $equipecontroller = new EquipeController();
        $usuarioequipecontroller = new UsuarioEquipeController();

        $idusuario = $usuariocontroller->buscaIdUsuario($_SESSION['usuario']->getEmail()); //busca o id do usuário
        $idprojeto = $projetocontroller->buscaIdProjeto($_POST['nomeProjeto'], $idusuario[0]); //busca o id do projeto
        $idequipe = $equipecontroller->criaNovaEquipe($_POST['nomeEquipe'], $idprojeto[0]); //cadastra a equipe e retorna seu id
        echo json_encode($usuarioequipecontroller->criaNovoUsuarioEquipe($idusuario[0], $idequipe[0], "Admin"));

    } else if($_POST['chave'] == "buscar-projetos"){
        $usuariocontroller = new UsuarioController();
        $projetocontroller = new ProjetoController();
        $usuarioprojetocontroller = new UsuarioProjetoController();
        $nomesprojetos = null;

        $idusuario = $usuariocontroller->buscaIdUsuario($_SESSION['usuario']->getEmail());
        $idprojeto = $usuarioprojetocontroller->buscaIdProjeto($idusuario[0]);
        foreach($idprojeto as $key => $value){
            $nomesprojetos[$key] = $projetocontroller->buscaNomeProjeto($value['idprojeto']);
        }
        echo json_encode($nomesprojetos);

    } else if($_POST['chave'] == "buscar-dados-projeto"){
        $usuariocontroller = new UsuarioController();
        $projetocontroller = new ProjetoController();
        $dados = null;

        $_SESSION['projeto'] = $projetocontroller->buscaDadosProjeto($_POST['nomeProjeto'], $_SESSION['usuario']->getId());
        $dadosproj = [ $_SESSION['projeto']->getNome(), $_SESSION['projeto']->getDescricao() ];
        echo json_encode($dadosproj);

    } else if($_POST['chave'] == "buscar-equipes"){
        $usuariocontroller = new UsuarioController();
        $projetocontroller = new ProjetoController();
        $equipecontroller = new EquipeController();

        //$idusuario = $usuariocontroller->buscaIdUsuario($_SESSION['usuario']->getEmail()); //busca o id do usuário
        //$idprojeto = $projetocontroller->buscaIdProjeto($_POST['nomeProjeto'], $idusuario[0]); //busca o id do projeto
        echo json_encode($equipecontroller->buscaEquipes($_SESSION['projeto']->getId()));

    } else if($_POST['chave'] == "adicionar-usuario"){
        $usuariocontroller = new UsuarioController();
        $usuarioequipecontroller = new UsuarioEquipeController();
        $equipecontroller = new EquipeController();
        $idusuario = $usuariocontroller->buscaIdUsuario($_POST['email']);
        if(isset($idusuario)){
            $idequipe = $equipecontroller->buscaIdEquipe($_POST['nomeequipe'], $_SESSION['projeto']->getId());
            echo json_encode($usuarioequipecontroller->criaNovoUsuarioEquipe($idusuario[0], $idequipe[0], $_POST['tipoparticipacao']));
            exit();
        }  else {
            echo "<script> console.log('else'); </script>";
        }

    } else if($_POST['chave'] == "buscar-usuarios-equipe"){
        $usuarioequipecontroller = new UsuarioEquipeController();
        $equipecontroller = new EquipeController();
        $idequipe = $equipecontroller->buscaIdEquipe($_POST['nomeequipe'], $_SESSION['projeto']->getId());
        echo json_encode($usuarioequipecontroller->buscaUsuariosEquipe($idequipe[0]));
        exit();
        
    } else if($_POST['chave'] == "adicionar-tarefa"){
        $equipecontroller = new EquipeController();
        $atividadecontroller = new AtividadeController();
        $idequipe = $equipecontroller->buscaIdEquipe($_POST['nomeequipe'], $_SESSION['projeto']->getId());
        echo json_encode($atividadecontroller->novaAtividade($_POST['nome'], $_POST['descricao'], $_POST['prazo'], $_POST['prioridade'], $idequipe[0]));
        exit();

    } else if($_POST['chave'] == "buscar-tarefas"){
        $equipecontroller = new EquipeController();
        $atividadecontroller = new AtividadeController();
        $idequipe = $equipecontroller->buscaIdEquipe($_POST['nomeequipe'], $_SESSION['projeto']->getId());
        echo json_encode($atividadecontroller->buscaAtividades($idequipe[0]));
    } else if($_POST['chave'] == "atribuir-responsavel"){ //Atribui um membro da equipe como responsável por uma tarefa
        $equipecontroller = new EquipeController();
        $atividadecontroller = new AtividadeController();
        $usuarioatividadecontroller = new UsuarioAtividadeController();
        $idequipe = $equipecontroller->buscaIdEquipe($_POST['nomeequipe'], $_SESSION['projeto']->getId());
        $idatividade = $atividadecontroller->buscaAtividade($_POST['nometarefa'],$idequipe[0]);
        echo json_encode($usuarioatividadecontroller->cadastraNovoUsuarioAtividade($_POST['idusuario'], $idatividade[0]));
    } else if($_POST['chave'] == "agendar-reuniao"){
        $reuniaocontroller = new ReuniaoController();
        if($reuniaocontroller->agendarReuniao($_POST['data'], $_POST['horario'])){
            $_SESSION['reuniao'] = $reuniaocontroller->buscaIdUltimaReuniao();
            echo true;
        }
        die();
    } else if($_POST['chave'] == "criar-ata"){
        $atacontroller = new AtaController();
        if($atacontroller->criarAta($_POST['descata'])){
            $idata = $atacontroller->buscaIdUltimaAta();

            $equipecontroller = new EquipeController();
            $idequipe = $equipecontroller->buscaIdEquipe($_POST['nomeequipe'], $_SESSION['projeto']->getId());

            $era = new EquipeReuniaoAtaController();
            if($era->criarEquipeReuniaoAta($idequipe[0], $_SESSION['reuniao']['idreuniao'], $idata[0])){
                echo true;
            }
        }
    } else if($_POST['chave'] == "gerar-relatorio"){
        if($_POST['tabela'] == "projeto"){
            $projetocontroller = new projetoController();
            $resultados = $projetocontroller->gerarRelatorio($_SESSION['projeto']->getId());
            $equipes[] = null;
            $usuarioequipe = null;
            $i = 0;
            foreach($resultados as $resultado){
                if($resultado['equipe'] != $equipes[$i-1]){
                    $equipes[$i] = $resultado['equipe'];
                    $i++;
                } 
            }
            $i = 0;
            $j = 0;
            foreach($resultados as $resultado){
                if($resultado['equipe'] == $equipes[$i]){
                    $usuarioequipe[$i][$j]['equipe'] = $resultado['equipe'];
                    $usuarioequipe[$i][$j]['usuario'] = $resultado['usuario'];
                    $usuarioequipe[$i][$j]['papel'] = $resultado['papel'];
                    $j++;
                } else {
                    $i++;
                    $j = 0;
                }
            }
            file_put_contents("../views/relatorioprojeto.pdf", criaRelatorioProjeto($resultados[0]['projeto'], $equipes, $usuarioequipe));
            echo json_encode("relatorioprojeto.pdf");


        } else if($_POST['tabela'] == "reuniao"){
            $reuniaocontroller = new ReuniaoController();
            $reunioes = $reuniaocontroller->gerarRelatorioReunioes($_SESSION['projeto']->getId());
            file_put_contents("../views/relatorioreunioes.pdf", criaRelatorioReunioes($reunioes[0]['projeto'], $reunioes));
            echo json_encode("relatorioreunioes.pdf");
        }
    } 