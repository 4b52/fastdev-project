<?php
    include_once '../dao/projetodao.php';
    include_once '../bean/projeto.class.php';

    class projetoController{
        public function criaNovoProjeto($nome, $descricao, $idcriador){
            $projetodao = new ProjetoDao();
            $projeto = new Projeto();
            $projeto->setNome($nome);
            $projeto->setDescricao($descricao);
            $projeto->setIdcriador($idcriador);
            $projetodao->cadastraNovoprojeto($projeto);
            return $projetodao->buscaIdProjeto($projeto);
            die();
        }

        public function buscaNomeProjeto($idprojeto){
            $projetodao = new ProjetoDao();
            $projeto = new Projeto();
            $projeto->setId($idprojeto);
            return $projetodao->buscaNomeProjeto($projeto);
        }

        public function buscaIdProjeto($nome, $idcriador){
            $projetodao = new ProjetoDao();
            $projeto = new Projeto();
            $projeto->setNome($nome);
            $projeto->setIdcriador($idcriador);
            return $projetodao->buscaIdProjeto($projeto);
        }

        public function buscaDadosProjeto($nome, $idusuario){
            $projetodao = new ProjetoDao();
            $projeto = new Projeto();
            $projeto->setNome($nome);
            $projeto->setIdcriador($idusuario);
            $dadosprojeto = $projetodao->buscaDadosProjeto($projeto);
            $projeto->setDescricao($dadosprojeto[1]);
            $projeto->setId($dadosprojeto[0]);
            return $projeto;
        }

        public function alteraProjeto($nome, $descricao, $id){
            $projetodao = new ProjetoDao();
            $projeto = new Projeto();
            $projeto->setNome($nome);
            $projeto->setDescricao($descricao);
            $projeto->setId($id);
            return $projetodao->alteraProjeto($projeto);
            die();
        }

        public function excluiProjeto($id){
            $projetodao = new ProjetoDao();
            $projeto = new Projeto();
            $projeto->setId($id);
            return $projetodao->excluiProjeto($projeto);
            die();
        }

        public function gerarRelatorio($id){
            $projetodao = new ProjetoDao();
            $projeto = new Projeto();
            $projeto->setId($id);
            return $projetodao->gerarRelatorio($projeto);
        }
    }