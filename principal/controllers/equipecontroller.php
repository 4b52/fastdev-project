<?php
    include_once '../dao/equipedao.php';
    include_once '../bean/equipe.class.php';

    class equipeController{
        public function criaNovaEquipe($nome, $idprojeto){
            $equipedao = new EquipeDao();
            $equipe = new Equipe();
            $equipe->setNome($nome);
            $equipe->setIdprojeto($idprojeto);
            $equipedao->cadastraNovaEquipe($equipe);
            return $equipedao->buscaIdEquipe($equipe);
            die();
        }

        public function buscaIdEquipe($nome, $idprojeto){
            $equipedao = new EquipeDao();
            $equipe = new Equipe();
            $equipe->setNome($nome);
            $equipe->setIdprojeto($idprojeto);
            return $equipedao->buscaIdEquipe($equipe);
        }

        public function buscaEquipes($idprojeto){
            $equipedao = new EquipeDao();
            $equipe = new Equipe();
            $equipe->setIdprojeto($idprojeto);
            return $equipedao->buscaEquipes($equipe);
        }

        public function alteraEquipe($nome, $idequipe, $idprojeto){
            $equipedao = new EquipeDao();
            $equipe = new Equipe();
            $equipe->setNome($nome);
            $equipe->setIdequipe($idequipe);
            $equipe->setIdprojeto($idprojeto);
            return $equipedao->alteraEquipe($equipe);
            die();
        }

        public function excluiEquipe($idequipe, $idprojeto){
            $equipedao = new EquipeDao();
            $equipe = new Equipe();
            $equipe->setIdequipe($idequipe);
            $equipe->setIdprojeto($idprojeto);  
            return $equipedao->excluiEquipe($equipe);
            die();
        }
    }