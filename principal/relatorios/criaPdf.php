<?php
    include '../../vendor/autoload.php';
    use Dompdf\Dompdf;

    function criaRelatorioProjeto($projeto, $equipes, $usuarioequipe){
        ob_start(); ?>
        <html>
            <head>
                <meta charset="utf-8">
                <title>Relatório de Projeto</title>
                <style>
                    h3, h4{
                        text-align: center;
                    }

                    .title-equipe{
                        width: 100%;
                        font-size: 12pt;
                        font-weight: bolder;
                        border-bottom: 3px solid rgba(118,	54, 193, 0.7);
                    }

                    .header{
                        width: 100%;
                    }

                    .content-header{
                        width: 50%;
                        float: left;
                        text-align: center;
                        border-bottom: 1px solid rgba(118,	54, 193, 0.7);
                    }

                    .membro-papel{   
                        font-size: 11pt;
                        border-bottom: 1px solid rgba(118,	54, 193, 0.7);
                        clear: left;
                    }
                </style>
            </head>
            <body>
                <h3> Relatório do Projeto <?php echo $projeto?></h3><br>
                <h4>Equipes e Membros</h4><br><br>
                <?php 
                    foreach($equipes as $e) {
                        echo "<div class='title-equipe'>Equipe de $e</div><br>";
                        echo "<div class='header'><div class='content-header'>Membro</div><div class='content-header'>Papel na Equipe</div></div>";
                        foreach($usuarioequipe as $key => $value){
                            foreach($value as $dados){
                                if($dados['equipe'] == $e) {
                                    echo "<div class='membro-papel'><div class='content-header'>".$dados['usuario']."</div><div class='content-header'>".$dados['papel']."</div></div><br><br>";
                                }
                            }
                        }
                    }
                ?>
            </body>
        </html>
<?php
        $html = ob_get_clean();
        /* Cria a instância */
        $dompdf = new DOMPDF();

        /* Carrega seu HTML */
        $dompdf->load_html($html);

        /* Renderiza */
        $dompdf->render();

        /* Exibe */
        return $dompdf->output();
        // return $dompdf->stream();
    }

    function criaRelatorioReunioes($projeto, $reunioes){
        ob_start();
?>
        <html>
            <head>
                <meta charset="utf-8">
                <title>Relatório de Projeto</title>
                <style>
                    h3, h4{
                        text-align: center;
                    }

                    .title-equipe{
                        width: 100%;
                        font-size: 12pt;
                        font-weight: bolder;
                        border-bottom: 3px solid rgba(118,	54, 193, 0.7);
                    }

                    .header{
                        width: 100%;
                    }

                    .content-header{
                        width: 33.3%;
                        float: left;
                        text-align: center;
                        border-bottom: 1px solid rgba(118,	54, 193, 0.7);
                    }

                    .membro-papel{   
                        font-size: 11pt;
                        border-bottom: 1px solid rgba(118,	54, 193, 0.7);
                        clear: left;
                    }
                </style>
            </head>
            <body>
                <h3> Relatório do Projeto <?php echo $projeto?></h3><br>
                <h4>Reuniões</h4><br><br>
                <?php 
                    echo "<div class='header'><div class='content-header'>Equipe</div><div class='content-header'>Data</div><div class='content-header'>Horário</div></div>";
                    foreach($reunioes as $dados) {
                        echo "<div class='membro-papel'><div class='content-header'>".$dados['equipe']."</div><div class='content-header'>".$dados['data']."</div><div class='content-header'>".$dados['horario']."</div></div><br><br>";
                    }
                ?>
            </body>
        </html>
<?php
    $html = ob_get_clean();
    /* Cria a instância */
    $dompdf = new DOMPDF();

    /* Carrega seu HTML */
    $dompdf->load_html($html);

    /* Renderiza */
    $dompdf->render();

    /* Exibe */
    return $dompdf->output();
    // return $dompdf->stream();
}
?>