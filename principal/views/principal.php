<?php
    include_once '../bean/usuario.class.php';
    include_once '../bean/projeto.class.php';
    include_once '../bean/equipe.class.php';
    include_once '../bean/atividade.class.php';
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../css/pg-principal.css">
    <script src="https://kit.fontawesome.com/5cd09c3eca.js" crossorigin="anonymous"></script>
    <title>Principal</title>
</head>
<body onload="trocaDiv(); buscaProjetos();">
    <div class="wrapper">
        <div class="menu">

            <div class="logo"></div>

            <div class="opcoesPrincipais">
                <i class="fas fa-bell"></i>

                <i class="fas fa-calendar-alt"></i>

                <i class="fas fa-bolt" onclick="trocaDiv();"></i>

                <i class="fas far fa-newspaper" id="relatorio" onclick="criarViewRelatorio();"></i>
            </div>

            <div class="opcoesSecundarias">
                <i class="fas fa-question-circle"></i>

                <div class="img-usuario" onclick="viewImgUserClick();"></div>
            </div>

        </div>
        <div class="corpo">
            <div id="noproject">
                <div id="ola">Bem-Vindo, <?php echo $_SESSION['usuario']->getNome();?></div>

                <div id="yourprojects">
                    <h3>No Momento Não Há Projetos Selecionados</h3>
                    <h4>Seus Projetos</h4>
                    <div class="project newproject">
                        <button class="botao botao-projeto" id="newproject">Novo Projeto</button>
                    </div>
                </div>
            </div>
            <div id="projatual">
                <div class="nome-projeto" id="title"></div>
                <div class="funcionalidades-projeto">
                    <i class="fas fa-folder" ></i> 
                    <i class="fas fa-folder-plus" id="criarequipe"></i>   
                    <i class="fas fa-search"></i>
                    <i class="fas fa-trash-alt"></i>
                    <i class="fas fa-ellipsis-h"></i>
                </div>
                <div id="descricao">
                    Descrição do projeto
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="../../node_modules/jquery/dist/jquery.min.js"></script>

    <script type="text/javascript">
        var nomeprojeto = null;
        function trocaDiv(){
            if(nomeprojeto != null){
                if($('#noproject').css('display') == 'block'){
                $('#noproject').css('display', 'none');
                $('#projatual').css('display', 'block');
                } else {
                    $('#noproject').css('display', 'block');
                    $('#projatual').css('display', 'none');
                }
            } else {
                $('#noproject').css('display', 'block');
                $('#projatual').css('display', 'none');
            }
        }

        $("#newproject").click(function(e) {
            e.preventDefault();
            var html = '<div class="novocadastro">'+
                            '<h3>Cadastrar Projeto</h3>'+
                            '<div class="campo">'+
                            '    <label for="nome">Nome do Projeto</label>'+
                            '    <input type="text" name="nome" id="nome">'+
                            '</div>'+
                            '<div class="campo">'+
                            '    <label for="descricao">Descrição do Projeto</label>'+
                            '    <input type="text" name="descricao" id="desc">'+
                            '</div>'+
                            '<button class="botao" id="criarcadastro" onclick="criarCadastroProjeto();">Criar Projeto</button>'+
                            '<button class="botao" id="cancelarcadastro" onclick="cancelarCadastro();">Cancelar</button>'+
                        '</div>';
            $('.corpo').append(html);
            $("#yourprojects").css("visibility", "hidden");
        });

        function criarCadastroProjeto(){
            $.ajax({
                type: "POST",
                url: "../controllers/index.php",
                data: { chave : "novo-projeto", nomeProjeto : $("#nome").val(), descricaoProjeto : $("#desc").val()},
                success: function(resultado){
                    if(resultado){
                        alert("Projeto criado com sucesso.");
                    } else{
                        alert("Este projeto já existe.");
                    }
                    
                },
                error: function(){
                    alert("Erro ao Criar o Projeto");
                }
            });
            buscaProjetos();
            $("#yourprojects").css("visibility", "visible");
            
            $(".corpo .novocadastro").remove();
        }

        $("#criarequipe").click(function(e){
            e.preventDefault();
            var html = '<div class="novocadastro">'+
                            '<h3>Cadastrar Equipe</h3>'+
                            '<div class="campo">'+
                            '    <label for="nome">Nome da Equipe</label>'+
                            '    <input type="text" name="nome" id="nome">'+
                            '</div>'+
                            '<button class="botao" id="criarcadastro" onclick="criarCadastroEquipe();">Criar Equipe</button>'+
                            '<button class="botao" id="cancelarcadastro" onclick="cancelarCadastro();">Cancelar</button>'+
                        '</div>';
            $('.corpo').append(html);
        });

        function criarCadastroEquipe(){
            $.ajax({
                type: "POST",
                url: "../controllers/index.php",
                data: { chave : "nova-equipe", nomeEquipe : $("#nome").val(), nomeProjeto : $("#title").text()},
                success: function(resultado){
                    if(resultado){
                        alert("Equipe cadastrada com sucesso.");
                    } else {
                        alert("Esta equipe já existe.");
                    }
                },
                error: function(){
                    alert("Erro ao cadastrar nova equipe");
                }
            });
            $(".corpo .novocadastro").remove();
            buscaEquipes();
        }

        function cancelarCadastro(){
            $(".corpo .novocadastro").remove();
            $("#yourprojects").css("visibility", "visible");
        }

        function buscaProjetos(){
            $("#yourprojects .projremove").remove();
            $.ajax({
                type: "POST",
                url: "../controllers/index.php",
                data: { chave : "buscar-projetos"},
                success: function(resultado){
                    var projetos = JSON.parse(resultado);
                    for(let i = 0; i < projetos.length; i++){
                        var nome = "'"+projetos[i][0][0]+"'";
                        var html = '<div class="project projremove"><button class="botao botao-projeto" id="'+projetos[i][0][0]+'" onclick="buscaDadosProjeto('+nome+');">'+projetos[i][0][0]+'</button></div>';
                        $("#yourprojects").append(html);
                    }
                },
                error: function(){
                    alert("Erro ao buscar os projetos");
                }
            });
        }

        function buscaDadosProjeto(nomeProjeto){
            $.ajax({
                type: "POST",
                url: "../controllers/index.php",
                data: { chave : "buscar-dados-projeto", nomeProjeto : nomeProjeto },
                success: function(dados){
                    var dadosproj = JSON.parse(dados);
                    trocaDiv();
                    $("#title").text(dadosproj[0]);
                    $("#descricao").text(dadosproj[1]);
                    nomeprojeto = dadosproj[0];
                    buscaEquipes();
                }, 
                error: function(){
                    alert("Erro ao buscar os dados do projeto");
                }
            });
        }

        function buscaEquipes(){
            $("#projatual .equipe").remove();
            $.ajax({
                type: "POST",
                url: "../controllers/index.php",
                data: { chave : "buscar-equipes", nomeProjeto : $("#title").text() },
                success: function(resultado){
                    var equipes = JSON.parse(resultado);
                    for(let i = 0; i < equipes.length; i++){
                        var nome = equipes[i][1];
                        nome = nome.replace(" ", "-");
                        var nomeparafuncao = "'"+nome+"'";
                        var chave = "'equipe'";
                        var html = '<div class="equipe">'+
                                        '<div class="nome-equipe" id="'+nome+'">'+equipes[i]['nome']+'</div>'+
                                        '<div class="funcionalidades-equipe">'+
                                            '<i class="fas fa-user"></i>'+
                                            '<i class="fas fa-user-plus" onclick="adicionarMembro('+nomeparafuncao+');"></i>'+
                                            '<i class="fas fa-tasks" onclick="adicionarTarefa('+nomeparafuncao+');"></i>'+
                                            '<i class="fas fa-handshake" onclick="marcarReuniaoView('+nomeparafuncao+');"></i>'+
                                            '<i class="fas fa-search"></i>'+
                                            '<i class="fas fa-trash-alt"></i>'+
                                        '</div>'+
                                        '<div id="tarefas-'+nome+'" class="membros">'+
                                        '</div>'+
                                    '</div>'
                        $("#projatual").append(html);
                        buscaTarefas(nome);
                    }
                },
                error: function(){
                    alert("Erro ao buscar os projetos");
                }
            });
        }

        function adicionarMembro(nomeequipe){
            nomeequipe = "'"+nomeequipe+"'";
            var html = '<div class="novocadastro">'+
                            '<h3>Adicionar Membro</h3>'+
                            '<div class="campo">'+
                            '    <label for="email">Email</label>'+
                            '    <input type="text" name="email" id="email">'+
                            '</div>'+
                            '<div class="campo">'+
                            '    <label for="tipoparticpacao">Papel na Equipe</label>'+
                            '    <input type="text" name="participacao" id="tipoparticipacao">'+
                            '</div>'+
                            '<button class="botao" id="criarcadastro" onclick="requisitaAdicaoMembro('+nomeequipe+');">Adicionar</button>'+
                            '<button class="botao" id="cancelarcadastro" onclick="cancelarCadastro();">Cancelar</button>'+
                        '</div>';
            $('.corpo').append(html);
        }

        function requisitaAdicaoMembro(nomeequipe){
            $.ajax({
                type: "POST",
                url: "../controllers/index.php",
                data: { chave : "adicionar-usuario", email : $("#email").val(), tipoparticipacao : $("#tipoparticipacao").val(), nomeequipe : $("#"+nomeequipe).text() },
                success: function(resultado){
                    if(JSON.parse(resultado)){
                        $(".corpo .novocadastro").remove();
                        alert("Membro adicionado com sucesso.");
                    } else {
                        alert("Este membro já está na equipe.");
                    }
                },
                error: function(){
                    alert("Erro ao buscar membro para inclusão");
                }
            });
        }

        function buscaMembros(nomeEquipe, nomeTarefa){
            let nomeequipefuncao = "'"+nomeEquipe+"'";
            let nometarefafuncao = "'"+nomeTarefa+"'";

            $("#projatual").append('<div class="att-responsavel">'+
                                        '<div class="title-resp">Selecione o responsável pela tarefa</div>'+
                                        '<div class="exit" onclick="cancelaResp();"><i class="fas fa-times-circle"></i></div>'+
                                        '<div class="colunas">'+
                                            '<div class="col-nome">Nome</div>'+
                                            '<div class="col-papel">Papel na Equipe</div>'+
                                        '</div>'+
                                    '</div>');
            $.ajax({
                type: "POST",
                url: "../controllers/index.php",
                data: { chave : "buscar-usuarios-equipe", nomeequipe : nomeEquipe },
                success: function(resultado){
                    var membros = JSON.parse(resultado);

                    for(let i = 0; i < membros.length; i++){
                        let idmembrofuncao = "'"+membros[i][0]+"'";
                        let html = '<div class="opt-resp" onclick="atribuirResponsavel('+nomeequipefuncao+','+nometarefafuncao+','+idmembrofuncao+');"><div class="nome-resp">'+membros[i][1]+'</div><div class="papel-resp">'+membros[i][2]+'</div></div>';
                        $(".att-responsavel").append(html); 
                    }
                },  
                error: function(erro){
                    console.log("Erro ao buscar usuários da equipe.\n"+JSON.stringify(erro));
                }
            });
        }

        function adicionarTarefa(nomeequipe){
            nomeequipe = "'"+nomeequipe+"'";
            var html = '<div class="novocadastro">'+
                            '<h3>Adicionar Tarefa</h3>'+
                            '<div class="campo">'+
                            '    <label for="nome">Nome</label>'+
                            '    <input type="text" name="nome" id="nome">'+
                            '</div>'+
                            '<div class="campo">'+
                            '    <label for="desc">Descrição</label>'+
                            '    <input type="text" name="desc" id="desc">'+
                            '</div>'+
                            '<div class="campo">'+
                            '    <label for="prazo">Prazo</label>'+
                            '    <input type="date" name="prazo" id="prazo">'+
                            '</div>'+
                            '<div class="campo">'+
                            '    <label for="prioridade">Prioridade</label>'+
                            '    <input type="text" name="prioridade" id="prioridade">'+
                            '</div>'+
                            '<button class="botao" id="criarcadastro" onclick="requisitaAdicaoTarefa('+nomeequipe+');">Adicionar</button>'+
                            '<button class="botao" id="cancelarcadastro" onclick="cancelarCadastro();">Cancelar</button>'+
                        '</div>';
            $('.corpo').append(html);
        }

        function requisitaAdicaoTarefa(nomeequipe){
            $.ajax({
                type: "POST",
                url: "../controllers/index.php",
                data: { chave : "adicionar-tarefa", nome : $("#nome").val(), descricao : $("#desc").val(), prazo : $("#prazo").val(), prioridade : $("#prioridade").val(), nomeequipe : $("#"+nomeequipe).text() },
                success: function(resultado){
                    if(JSON.parse(resultado)){
                        $(".corpo .novocadastro").remove();
                        alert("Tarefa adicionada com sucesso.");
                    } else {
                        alert("Esta tarefa já existe na equipe ou os dados são incompatíveis.");
                    }
                },
                error: function(){
                    alert("Erro ao tentar incluir tarefa.");
                }
            });
            buscaTarefas(nomeequipe);

        }

        function buscaTarefas(nomeEquipe){
            $("#tarefa-"+nomeEquipe+" .membro").remove();
            var html2 = null;
            $.ajax({
                type: "POST",
                url: "../controllers/index.php",
                data: { chave : "buscar-tarefas" , nomeequipe : $("#"+nomeEquipe).text() },
                success: function(resultado){
                    var tarefas = JSON.parse(resultado);
                    let nomeequipefuncao = "'"+$("#"+nomeEquipe).text()+"'";
                    for(let i = 0; i < tarefas.length; i++){
                        let nometarefafuncao = "'"+tarefas[i][1]+"'";
                        html2 = '<div class="membro">'+
                                    '<div class="nome">'+tarefas[i][1]+'</div>'+
                                    '<div class="img-membro" onclick="buscaMembros('+nomeequipefuncao+', '+nometarefafuncao+');"></div>'+
                                    '<div class="funcionalidades-membro">'+
                                        '<div class="conversar"><i class="far fa-comment-dots"></i></div>'+
                                    '</div>'+
                                '</div>';
                        $("#tarefas-"+nomeEquipe).append(html2);
                    }
                },
                error: function(){
                    console.log("Erro ao buscar as tarefas das equipes");
                }
            });
        }

        function atribuirResponsavel(nomeEquipe, nomeTarefa, idUsuario){
            $.ajax({
                type: "POST",
                url: "../controllers/index.php",
                data: { chave : "atribuir-responsavel", nomeequipe : nomeEquipe, nometarefa : nomeTarefa, idusuario :idUsuario },
                success: function(resultado){
                    if(JSON.parse(resultado)){
                        alert("Responsável atribuído com sucesso.");
                    } else {
                        alert("Erro ao atribuir o responsável.");
                    }
                    cancelaResp();
                },
                error: function(erro){
                    console.log("Erro no servidor ao atribuir um responsável.\n"+JSON.stringify(erro));
                }
            });
        }

        function cancelaResp(){
            $("#projatual .att-responsavel").remove();
        }

        function marcarReuniaoView(equipe){
            var nomeequipe = "'"+equipe+"'";
            var html = '<div class="novocadastro">'+
                            '<h3>Agendar Reunião</h3>'+
                            '<div class="campo">'+
                            '    <label for="data">Data</label>'+
                            '    <input type="date" name="data" id="data">'+
                            '</div>'+
                            '<div class="campo">'+
                            '    <label for="horario">Hora</label>'+
                            '    <select name="horario" id="horario">'+
                            '       <option value="08:00">08:00</option>'+
                            '       <option value="08:30">08:30</option>'+
                            '       <option value="09:00">09:00</option>'+
                            '       <option value="09:30">09:30</option>'+
                            '       <option value="10:00">10:00</option>'+
                            '       <option value="10:30">10:30</option>'+
                            '       <option value="11:00">11:00</option>'+
                            '       <option value="11:30">11:30</option>'+
                            '       <option value="13:30">13:30</option>'+
                            '       <option value="14:00">14:00</option>'+
                            '       <option value="14:30">14:30</option>'+
                            '       <option value="15:00">15:00</option>'+
                            '       <option value="15:30">15:30</option>'+
                            '       <option value="16:00">16:00</option>'+
                            '       <option value="16:30">16:30</option>'+
                            '       <option value="17:00">17:00</option>'+
                            '       <option value="17:30">17:30</option>'+
                            '       <option value="18:00">18:00</option>'+
                            '    </select>'+
                            '</div>'+
                            '<button class="botao" id="criarcadastro" onclick="agendarReuniao('+nomeequipe+');">Agendar</button>'+
                            '<button class="botao" id="cancelarcadastro" onclick="cancelarCadastro();">Cancelar</button>'+
                        '</div>';
            $('.corpo').append(html);
        }

        function agendarReuniao(nomeequipe){
            let sucesso = false;
            var equipe = $("#"+nomeequipe).text();
            $.ajax({
                type: "POST",
                url: "../controllers/index.php",
                data: { chave : "agendar-reuniao", data : $("#data").val(), horario : $("#horario").val() },
                success: function(resultado){
                    if(resultado){
                        $(".corpo .novocadastro").remove();
                        alert("Reunião agendada com sucesso.");
                        criarAtaView(equipe);
                    } else {
                        alert("Já existe uma reunião agendada para o dia e horário selecionados.");
                    }
                },
                error: function(){
                    alert("Erro ao tentar agendar reunião.");
                }
            });
            
        }

        function criarAtaView(equipe){
            let nomeequipe = "'"+equipe+"'";
            var html = '<div class="novocadastro">'+
                            '<h3>Criar Ata</h3>'+
                            '<div class="campo">'+
                            '    <label for="descata">Descrição</label>'+
                            '    <input type="text" name="descata" id="descata">'+
                            '</div>'+
                            '<button class="botao" id="criarcadastro" onclick="criarAta('+nomeequipe+');">Criar Ata</button>'+
                            '<button class="botao" id="cancelarcadastro" onclick="cancelarCadastro();">Cancelar</button>'+
                        '</div>';
            $('.corpo').append(html);
        }

        function criarAta(equipe){
            console.log(equipe);
            $.ajax({
                type: "POST",
                url: "../controllers/index.php",
                data: { chave : "criar-ata", descata : $("#descata").val(), nomeequipe : equipe },
                success: function(resultado){
                    if(JSON.parse(resultado)){
                        $(".corpo .novocadastro").remove();
                        alert("Ata criada com sucesso.");
                    } else {
                        alert("Já existe uma ata para esta reuniao.");
                    }
                },
                error: function(){
                    alert("Erro ao tentar criar a ata.");
                }
            });
        }

        function criarViewRelatorio(){
            var html = '<div class="novocadastro gerarrelatorio">'+
                            '<h3>Gerar Relatório</h3>'+
                            '<div class="exit" onclick="cancelarCadastro();"><i class="fas fa-times-circle"></i></div>'+
                            '<button class="botao botao-relatorio" id="relatorioprojeto" onclick="gerarRelatorio('+"'projeto'"+');">Relatório de Equipes e Membros</button>'+
                            '<button class="botao botao-relatorio" id="relatorioreuniao" onclick="gerarRelatorio('+"'reuniao'"+');">Relatório de Reunião</button>'+
                        '</div>';
            $('.corpo').append(html);
        } 

        function gerarRelatorio(objeto){
            $.post("../controllers/index.php",
                {
                    chave : "gerar-relatorio", 
                    tabela : objeto
                }
             ).done(function(arquivo){
                window.open(JSON.parse(arquivo), '_blank');
            });
        }

        function viewImgUserClick(){
            var html = '<div class="novocadastro gerarrelatorio">'+
                            '<h3>Opções do Usuário</h3>'+
                            '<div class="exit" onclick="cancelarCadastro();"><i class="fas fa-times-circle"></i></div>'+
                            '<button class="botao" onclick="logout();">Sair</button>'+
                        '</div>';
            $('.corpo').append(html);
        }

        function logout(){
            let r = confirm("Você deseja mesmo sair?");
            if(r){
                
                window.location.href = 'inicial.php';
            }
        }
        
    </script>
</body>
</html>