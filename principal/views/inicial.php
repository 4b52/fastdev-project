<?php
    require "../../vendor/autoload.php";
    $google = new \League\OAuth2\Client\Provider\Google(GOOGLE);
    $authUrl = $google->getAuthorizationUrl();
    session_start();

    if(isset($_SESSION['usuario'])){
        unset($_SESSION['usuario']);
    }

    if(isset($_SESSION['projeto'])){
        unset($_SESSION['projeto']);
    }

    if(empty($_SESSION['userlogin'])){
        $google = new \League\OAuth2\Client\Provider\Google(GOOGLE);
        $authUrl = $google->getAuthorizationUrl();
        $error = filter_input(INPUT_GET, 'error');
        $code = filter_input(INPUT_GET, 'code');

        if($error){
            echo "<h3>Você precisa autorizar para continuar</h3>";
        }

        if($code){
            $token = $google->getAccessToken("authorization_code",["code"=>$code]);
            $_SESSION['userlogin'] = serialize($google->getResourceOwner($token));
            header("Location: ".GOOGLE["redirectUri"]);
            die();
        }
    } else {
        header("Location: ../controllers/index.php");
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="google-signin-client_id" content="715445038742-8d95mbiodader4cn2ll65ksme4598jf8.apps.googleusercontent.com">
    <link rel="stylesheet" href="../../css/pg-inicial.css">
    <title>FastDev</title>
</head>
<body onload="trocaDiv('login');">
    <div class="wrapper">
        <div class="header">
            <div class="logo"></div>

            <div class="menu">
                <ul class="opcoes">
                    <li class="opcao" onclick="trocaDiv('cadastro');">Cadastrar-se</li>
                    <li class="opcao" onclick="trocaDiv('login');">Login</li>
                </ul>
            </div>
        </div>

        <div class="corpo">
            <div class="op" id="login">
                <h3>LOGIN</h3>
                <form action="../controllers/index.php" method="post">
                    <div class="campo">
                        <label for="email">Email</label>
                        <input type="email" name="email" placeholder="Digite aqui seu email">
                    </div>

                    <div class="campo">
                        <label for="senha">Senha</label>
                        <input type="password" name="senha" placeholder="Digite aqui sua senha">
                    </div>
                    <button class="botao google-login"><a title='Logar com o Google' name="google-login" value="google-login" href="<?php echo $authUrl?>">Google Login</a></button>

                    <input type="submit" value="Acessar" name="acessar" class="botao">
                </form>
            </div>

            <div class="op" id="cadastro">
                <h3>CADASTRAR-SE</h3>
                <form action="../controllers/index.php" method="post">
                    <div class="campo">
                        <label for="nome">Nome</label>
                        <input type="text" name="nome" placeholder="Digite aqui como devemos chamá-lo">
                    </div>

                    <div class="campo">
                        <label for="email">Email</label>
                        <input type="email" name="email" placeholder="Insira seu melhor email">
                    </div>

                    <div class="campo">
                        <label for="senha">Senha</label>
                        <input type="password" name="senha" placeholder="Digite sua senha">
                    </div>

                    <div class="campo">
                        <label for="repetirsenha">Repetir Senha</label>
                        <input type="password" name="repetirsenha" placeholder="Repita sua senha">
                    </div>

                    <input type="submit" value="Cadastrar-se" name="cadastrarse" class="botao">
                </form>
            </div>
        </div>
    </div>

    <script src="../../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>


    <script>
        function trocaDiv(id){
            var divs = document.getElementsByClassName('op');
            for(let i = 0; i < divs.length; i++){
                let idaux = divs[i].id;
                if(idaux == id){
                    $("#"+idaux).show();
                } else {
                    $("#"+idaux).hide();
                }
            }
        }

    </script>
</body>
</html>